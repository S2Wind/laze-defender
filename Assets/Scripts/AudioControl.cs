﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    [SerializeField] AudioClip fire;
    [SerializeField] AudioClip explosion;
    [SerializeField] [Range(0, 1)] float volFire = 0.3f;
    AudioSource audio;
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    public void PlayFireSound()
    {
        AudioSource.PlayClipAtPoint(fire, Camera.main.gameObject.transform.position, volFire);
    }
    public void PlayExplosion()
    {
        AudioSource.PlayClipAtPoint(explosion, Camera.main.gameObject.transform.position, 2f);
    }
}
